#!/usr/bin/env python3

from imapclient import IMAPClient
import email
import sys
import os
import traceback
import time
import logging

logging.basicConfig(
    format='%(asctime)s - %(levelname)s: %(message)s',
    level=logging.INFO
)

# imapclient
# https://imapclient.readthedocs.io/en/2.1.0/
# IDLE support: https://github.com/mjs/imapclient/blob/master/imapclient/imapclient.py#L743
# Watching a Mailbox Using IDLE: https://imapclient.readthedocs.io/en/2.1.0/advanced.html#watching-a-mailbox-using-idle

# SERVER=imap.gmail.com EMAIL=<user>@gmail.com PASSWORD=<password> ./imapclient_idle.py
if __name__ == '__main__':
    server = os.environ['SERVER']
    user = os.environ['EMAIL']
    password = os.environ['PASSWORD']
    from_email = os.environ['FROM_EMAIL']

    # Note that IMAPClient does not handle low-level socket errors that can happen when maintaining long-lived TCP connections. 
    # Users are advised to renew the IDLE command every 10 minutes to avoid the connection from being abruptly closed.
    renew_idle_time_s = 600

    try:
        server = IMAPClient(server)
        capabilities = server.login(user, password)
        print("Capabilities", capabilities)

        select_info = server.select_folder('INBOX')
        print('There are {} messages in INBOX'.format(select_info[b'EXISTS']))

        messages = server.search(['FROM', from_email])
        print("{} messages from me: {}".format(len(messages), messages))

        # Start IDLE mode
        server.idle()
        idling = True
        old_idle_time_s = time.time()
        print("Connection is now in IDLE mode, send yourself an email or quit with ^c")

        while True:
            try:
                # Wait for up to 30 seconds for an IDLE response
                print("Before idle check")
                responses = server.idle_check(timeout=10)
                print("After idle check")

                if responses:
                    uids = []
                    for response in responses:
                        uid, msg = response
                        if msg == b"EXISTS":
                            uids.append(uid)

                    if uids:
                        server.idle_done()
                        idling = False

                        #server.select_folder('INBOX')
                        #messages = server.search('UNSEEN')
                        #print("messages: {}".format(messages))
                        #sys.exit(0)

                        print("Fetching messages: {}".format(uids))
                        fetch_response = server.fetch(uids, ['RFC822'])
                        print(fetch_response)

                        for msg_id, message_data in fetch_response.items():
                            email_message = email.message_from_bytes(message_data[b'RFC822'])
                            print(msg_id, email_message.get('From'), email_message.get('Subject'))

                            if email_message.is_multipart():
                                for part in email_message.walk():
                                    ctype = part.get_content_type()
                                    cdispo = str(part.get('Content-Disposition'))

                                     # skip any text/plain (txt) attachments
                                    if ctype == 'text/plain' and 'attachment' not in cdispo:
                                        body = part.get_payload(decode=True).decode()
                                        print(body)
                                        break
                            else:
                                print(email_message.get_payload(decode=True).decode())
                        server.idle()
                else:
                    print("Nothing")

                new_idle_time_s = time.time()
                if idling and (new_idle_time_s - old_idle_time_s) >= renew_idle_time_s:
                    server.idle_done()
                    print("IDLE mode done")
                    server.idle()
                    old_idle_time = new_idle_time_s
                    print("IDLE mode renewed")
            except KeyboardInterrupt:
                if idling:
                    server.idle_done()
                break
    except Exception as e:
        exc_type, exc_value, exc_tb = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_tb)
        sys.exit(1)
    finally:
        server.logout()
