# [IMAP IDLE](https://en.wikipedia.org/wiki/IMAP_IDLE) playground

- [IMAP4 IDLE command (RFC 2177)](https://tools.ietf.org/html/rfc2177.html)
- [Using IMAP IDLE to Wait for Updates]()

> The concept is simple: the client connects to the server, selects a mailbox and enters the IDLE mode.  
> At this point the server sends notifications whenever something happens in the selected mailbox until the client ends the IDLE mode by issuing a DONE command.  

## IMAP client libraries

### imaplib
- https://pymotw.com/3/imaplib/
- https://docs.python.org/3/library/imaplib.html
- https://github.com/python/cpython/blob/3.7/Lib/imaplib.py
- https://stackoverflow.com/questions/18103278/how-to-check-whether-imap-idle-works

> IMAP4, uses clear text sockets  
> IMAP4_SSL uses encrypted communication over SSL sockets  
> IMAP4_stream uses the standard input and standard output of an external command  

### imaplib2
Threaded Python IMAP4 client version.

- https://github.com/imaplib2/imaplib2

### imapclient
-  https://imapclient.readthedocs.io/en/2.1.0/
- IDLE support: https://github.com/mjs/imapclient/blob/master/imapclient/imapclient.py#L743
-  Watching a Mailbox Using IDLE: https://imapclient.readthedocs.io/en/2.1.0/advanced.html#watching-a-mailbox-using-idle

### imbox
- https://github.com/martinrusev/imbox
- Missing [Push notification support](https://github.com/martinrusev/imbox/issues/1)

## Email Parsing
- https://docs.python.org/3/library/email.parser.html
- https://python.readthedocs.io/en/stable/library/email.examples.html
- https://github.com/mailgun/flanker

Manual parsing:  
- https://gist.github.com/ktmud/cb5e3ca0222f86f5d0575caddbd25c03  
- https://parseur.com/how-to/create-email-parser/  
