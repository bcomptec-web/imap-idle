#!/usr/bin/env python3

import imaplib
import sys
import traceback
from logging import LoggerAdapter, getLogger

logger = getLogger(__name__)

def idle(connection):
    tag = connection._new_tag().decode()
    idle_command = "{} IDLE\r\n".format(tag).encode()
    connection.send(idle_command)
    
    response = connection.readline()
    print("Response", response)
    
    connection.loop = True
    if response == b'+ idling\r\n':
        print("Waiting for new mail...")
        while connection.loop:
            resp = connection.readline().decode()
            uid, message = resp[2:-2].split(' ')
            yield uid, message
    else:
        raise Exception("IDLE not handled? : %s" % response)


def done(connection):
    connection.send("DONE\r\n".encode())
    connection.loop = False


# ...and IDLE
if 'IDLE' not in imaplib.Commands:
    imaplib.Commands['IDLE'] = ('NONAUTH', 'AUTH', 'SELECTED')

imaplib.Debug = 5
imaplib.IMAP4_PORT = 143
imaplib.IMAP4_SSL_PORT = 993

imaplib.IMAP4.idle = idle
imaplib.IMAP4.done = done


def get_header(connection, uid):
    _, msg_data = connection.fetch(uid, '(BODY.PEEK[HEADER])')
    for response_part in msg_data:
        if isinstance(response_part, tuple):
            return response_part[1].decode()


def get_body(connection, uid):
    _, msg_data = connection.fetch(uid, '(BODY.PEEK[TEXT])')
    for response_part in msg_data:
        if isinstance(response_part, tuple):
            return response_part[1].decode()
    return None


def get_flags(connection, uid):
    _, msg_data = connection.fetch(uid, '(FLAGS)')
    for response_part in msg_data:
        return imaplib.ParseFlags(response_part)


# SERVER=imap.gmail.com EMAIL=<user>@gmail.com PASSWORD=<password> ./imaplib_idle.py
if __name__ == '__main__':
    import os
    user = os.environ['EMAIL']
    password = os.environ['PASSWORD']
    server = os.environ['SERVER']

    try:
        conn = imaplib.IMAP4_SSL(server)

        typ, data = conn.login(user, password)
        print(typ, data)
        
        tpy, data = conn.select("INBOX")
        print(typ, data)
        
        num_msgs = int(data[0])
        print('There are {} messages in INBOX'.format(num_msgs))
        
        loop = True

        while loop:
            for uid, msg in conn.idle():
                print(uid, msg)
                if msg == "EXISTS":
                    conn.done()

                    print('HEADER:')
                    print(get_header(conn, uid))

                    print('\nBODY TEXT:')
                    print(get_body(conn, uid))

                    print('\nFLAGS:')
                    print(get_flags(conn, uid))
        conn.logout()
    except Exception as e:
        exc_type, exc_value, exc_tb = sys.exc_info()
        traceback.print_exception(exc_type, exc_value, exc_tb)
        sys.exit(1)
